# Coding challenge
This page consists a coding challenge for Data Engineering roles at Digital Healthcare Solution.

# Purpose
Aim of this test is three fold,

- evaluate your coding abilities 
- judge your technical experince
- understand how you design a solution

# How you will be judged
You will be scored on,

- coding standard, comments and style
- unit testing strategy
- overall solution design
- appropriate use of source control

# Intructions

- Please use hosted storage like [S3](https://aws.amazon.com/s3/?did=ft_card&trk=ft_card) for storing the results of the scraped documents
- Please use csv file for storing the results of the scraped documents
- Please use hosted PostgreSQL like [RDS](https://aws.amazon.com/rds/?did=ft_card&trk=ft_card) for storing keywords analytics
- Candidate should put their test results on a public code repository hosted on Github
- Once test is completed please share the Github repository URL to hiring team so they can review your work
- You are building a backend application and no UI is required, input can be provided using a configuration file or command line

# Challenge - News Content Collect and Store

Create a solution that crawls for articles about `coronavirus` from a news website, cleanses the response, stores in a cloud storage.

## Details

- Write an application to crawl an online news website, e.g. www.theguardian.com/au or www.bbc.com using a crawler framework such as [Scrapy] (http://scrapy.org/). You can use a crawl framework of your choice and build the application in Python.
- The appliction should cleanse the articles to obtain only information relevant to the news story, e.g. article text, author, headline, article url, etc.  Use a framework such as Readability to cleanse the page of superfluous content such as advertising and html
- Store the data in a hosted storage, e.g. aws-s3.

## Bonus problem - keywords analytics about coronavirus

Generate analytics from collected data. like word hit count.
### Details
- Use hosted PostgreSQL for storing analytics data.

